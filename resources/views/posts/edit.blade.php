{{-- @extends directive lets you "extend" a template, which defines its own sections. ex: navbar --}}
@extends('layouts.app')

{{-- @section directive is inject content layout from extended blade layout and display in child blade.  --}}
@section('content')
    <form method="POST" action="/posts/{{$post->id}}">
		{{-- 
			- CSRF stands for Cross-Site Request Forgery
			- A form of attack where malicious users may send malicious requests while pretending to be the authorized user.
			 - using @csrf laravel uses tokens to detect if form input requests have not been tampered with. This tokens are autogenerated for every client request.
		--}}
        @method('PUT')
		@csrf
    	<div class="form-group">
			<label for="title">Title</label>
			<input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
		</div>
		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" id="content" name="content" rows="3">{{$post->content}}</textarea>
		</div>
		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Update Post</button>
		</div>
	</form>
@endsection